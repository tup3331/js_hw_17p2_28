const timer = document.getElementById("timer");
const btn = document.getElementById("timer-button");

let time;
btn.addEventListener("click", () => {
    time = setTimeout(timerSucc, 3000);
});

function timerSucc() {
  timer.textContent = "Операція виконана успішно.";
};


///////////////////////////////////////////////////


const countdown = document.getElementById("countdown");

function timerCountdown() {
  let time = 10;

  timer.textContent = time;

  const interval = setInterval(() => {
    time--;
    timer.textContent = time;

    if (time <= 0) { //! Або time <= 0.99, якщо треба одразу після досягненя одиниці
      timer.textContent = "Зворотній відлік завершено.";
      clearInterval(interval);
    };


  }, 1000)
};

countdown.addEventListener("click", timerCountdown);